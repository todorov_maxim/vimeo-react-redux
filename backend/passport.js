const passport = require('passport')
const LocalStrategy = require('passport-local').Strategy;
const User = require('./mongo/schemes/user');

passport.serializeUser((user, done) => {
    done(null, user.id);
});

passport.deserializeUser(function (id, done) {
    console.log('DESERIALIZE', id);
    User.findById(id, function(err, user) {
        console.log(user);
        done(err, user);
    });
});

passport.use(new LocalStrategy({usernameField: 'email', passwordField: 'password'},
    (email, password, done) => {
        console.log(email, password);
        User.findOne({email: email}, (err, user) => {
            if (err) {
                return done(err);
            }
            if (
                !user
            ) {
                return done(null, false, {message: 'Incorrect email.'});
            }
            if (!user.validPassword(password)) {
                return done(null, false, {message: 'Incorrect password.'});
            }
            return done(null, user);
        })
        ;
    }
));