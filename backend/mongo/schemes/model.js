const mongoose = require('mongoose');


let modelSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    brandId: String,
    name: String,
    file: String,
    pdf: String,
    description: String,
    img: String,
    hierarchy: Array
});


const Model = mongoose.model('Model', modelSchema);

module.exports = Model;