const salt = require('../key');
const mongoose = require('mongoose');
const crypto = require('crypto');

let userSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    type: Number,
    email: String,
    company: String,
    brand: String,
    country: String,
    city: String,
    zip: String,
    address: String,
    username: String,
    password: String,
    logo: String,
    firstName: String,
    middleName: String,
    lastName: String
});

userSchema.methods.generateHash = function(password){
    return crypto.createHash('md5').update(password+salt).digest("hex");
};

userSchema.methods.validPassword = function(password){
    return (this.password === crypto.createHash('md5').update(password+salt).digest("hex"));
};

const User = mongoose.model('User', userSchema);

module.exports = User;