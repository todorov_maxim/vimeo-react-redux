const mongoose = require('mongoose');


let companySchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    userId: String,
    name: String,
    brands: String
});


const Comapany = mongoose.model('Company', companySchema);

module.exports = Comapany;