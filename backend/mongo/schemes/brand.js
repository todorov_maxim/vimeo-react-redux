const mongoose = require('mongoose');


let brandSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    companyId: String,
    name: String,
    models: Array
});


const Brand = mongoose.model('Brand', brandSchema);

module.exports = Brand;