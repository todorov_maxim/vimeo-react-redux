const express = require('express');
const session = require('express-session');
const FileStore = require('session-file-store')(session);
const passport = require('passport');
const cors = require('cors');
const fs = require('fs');
const path = require('path');
const multer = require('multer');
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads/')
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname)
    }
});
const upload = multer({storage: storage});


//helpers
const moveFile = (file, dir2, type) => {
    //gets file name and adds it to dir2 + rename file
    let f = path.basename(file);
    let split = f.split('.');
    f = type + '.' + split[split.length - 1];
    let dest = path.resolve(dir2, f);

    fs.rename(file, dest, (err) => {
        if (err) throw err;
        else console.log('Successfully moved');
    });
};


const moveFileToFolder = (id, filename, type) => {

    // check if we have users folder
    if (!fs.existsSync('./uploads/users/')) {
        fs.mkdirSync('./uploads/users/');
    }
    if (!fs.existsSync('./uploads/users/' + id)) {
        fs.mkdirSync('./uploads/users/' + id);
    }
    if (!fs.existsSync('./uploads/users/' + id + '/files/')) {
        fs.mkdirSync('./uploads/users/' + id + '/files/');
    }

    moveFile('./uploads/' + filename, './uploads/users/' + id + '/files/', type);
};

const CreateCompany = (id, userId, companyName, brandId) => {

    let company = new Company({
        _id: id,
        userId: userId,
        name: companyName,
        brands: brandId
    });

    company.save((err) => {
        if (err) throw err;

        console.log('Company saved.');
    });
};

const CreateBrand = (id, companyId, brandName) => {

    let brand = new Brand({
        _id: id,
        companyId: companyId,
        name: brandName,
        models: []
    });

    brand.save((err) => {
        if (err) throw err;

        console.log('Brand saved.');
    });
};

// Mongoose
const mongoose = require('mongoose');
const User = require('./mongo/schemes/user');
const Company = require('./mongo/schemes/company');
const Brand = require('./mongo/schemes/brand');
const Model = require('./mongo/schemes/model');

const app = express();
const port = 9099;

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(session({
    secret: 'bmw320',
    store: new FileStore(),
    cookie: {
        path: '/',
        httpOnly: true,
        maxAge: 60 * 60 * 1000,
    },
    resave: false,
    saveUninitialized: false
}));


// MONGO CONNECT
mongoose.connect('mongodb://127.0.0.1:27017/local', {useNewUrlParser: true});
let db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => {
    console.log('Successfully connected');
});


// signUp
app.post('/signUp', upload.single('logo'), (req, res) => {

    console.log(req.body);

    User.findOne({email: req.body.email}, (err, user) => {
        if (!user) {
            let extension;
            if (req.file) {
                extension = req.file.originalname.split('.')[1];
            }
            let id = new mongoose.Types.ObjectId();
            let user = new User({
                _id: id,
                logo: req.file ? './uploads/users/' + id + '/files/logo.' + extension : '',
                ...req.body
            });
            user.password = user.generateHash(req.body.password);
            user.save((err) => {
                if (err) throw err;

                console.log('Order successfully saved.');

                // move logo to user folder
                if (req.file) {
                    moveFileToFolder(id, req.file.originalname, 'logo');
                }

                // Check for company
                if (req.body.type === '2') {
                    let companyId = new mongoose.Types.ObjectId();
                    let brandId = new mongoose.Types.ObjectId();
                    CreateCompany(companyId, id, req.body.company, brandId);
                    CreateBrand(brandId, companyId, req.body.brand);
                }

                return res.json({
                    "success": true
                });
            });
        } else {
            res.setHeader('Content-Type', 'application/json');
            return res.json({
                "success": false,
                "message": "Such email have been already declared"
            });
        }
    });


    // res.sendStatus(200);

});


// getModels
app.post('/getModels', (req, res) => {
    console.log('Get Models',req.body);

    const userId = req.body.id;

    Company.find({userId}, (err, company_date)=>{
        const _id = company_date.brands;

        Brand.find(_id, (err, brand_date)=>{
            const models = brand_date[0].models;
            console.log('Find IT + ', brand_date[0].models);

            if(models.length > 0){

            }else{
                return res.json({
                    message: 'No models yet'
                });
            }

        });

    });
});



// PASPORT JS
require('./passport');
app.use(passport.initialize());
app.use(passport.session());
app.post('/login', (req, res, next) => {
    console.log('jjj');
    passport.authenticate('local', {failureFlash: 'Invalid username or password.'}, (err, user) => {
        if (err)
            return next(err);
        if (!user) {
            return res.sendStatus(401);
        }
        req.login(user, (err) => {
            if (err)
                return next(err);

            return res.sendStatus(200);
        })
    })(req, res, next);
});

const auth = (req, res, next) => {
    console.log(req.session);
    if (req.isAuthenticated())
        next();
    else
        return res.sendStatus(401);
}

app.get('/check', auth, (req, res) => {
    res.sendStatus(200);
})

app.get('/admin', auth, (req, res) => {
    res.send('admin page');
})

app.get('/logout', (req, res) => {
    req.logout();
    res.redirect();
});

app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname, 'build', 'index.html'));
});

// APP START
app.listen(port, () => {
    console.log('server start on port 9099');
});