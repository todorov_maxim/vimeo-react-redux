import {PerspectiveCamera, Scene, WebGLRenderer, HemisphereLight, OrbitControls} from "three-full";

export default class Viewer{
	constructor(id){
		this.init(id);
	}

	init(id){
		// CONTAINER
		this.container = document.getElementById( id );
		// this.container.id = 'threeJS';
		// document.body.appendChild( this.container );

		// CAMERA
		this.camera = new PerspectiveCamera( 45, window.innerWidth / window.innerHeight, 1, 2000 );
		this.camera.position.z = 50;

		// SCENE
		this.scene = new Scene();

		// RENDERER
		this.renderer = new WebGLRenderer( { antialias: true, alpha: true } );
		this.renderer.setPixelRatio( window.devicePixelRatio );
		this.renderer.setSize( window.innerWidth, window.innerHeight );
		this.renderer.shadowMap.enabled = true;

		// LIGHT
		this.initLight();

		// CONTROLS
		this.initControls();

		// ADD ON SCENE
		this.container.appendChild( this.renderer.domElement );

		// RESIZE EVENT
		window.addEventListener('resize', this.onWindowResize.bind(this));

		this.animate()
	}

	initControls(){
		this.controls = new OrbitControls( this.camera, this.renderer.domElement );
	}

	initLight(){
		let light = new HemisphereLight( 0xffffff, 0x444444 );
		light.name = 'HemisphereLight';
		light.position.set( 0, 300, 0 );
		this.scene.add( light );
	}

	onWindowResize() {
		this.camera.aspect = window.innerWidth / window.innerHeight;
		this.camera.updateProjectionMatrix();
		this.renderer.setSize( window.innerWidth, window.innerHeight );
	}

	animate(){
		requestAnimationFrame( ()=>{
			this.animate()
		});

		this.controls.update();
		this.renderer.render( this.scene, this.camera );
	}
}