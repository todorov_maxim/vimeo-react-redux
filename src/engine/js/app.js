import Viewer from './viewer/viewer';
import Loader from './loaders/loader';
import Loading from './helpers/loading';
import {SphereGeometry, MeshBasicMaterial, Mesh} from "three-full";
import '../styles/index.scss';

export default class App {
    constructor(api, id) {
        this.api = api;
        this.viewer = new Viewer(id);
        this.loading = new Loading(this.viewer.container);


        let geometry = new SphereGeometry(5, 32, 32);
        let material = new MeshBasicMaterial({color: 0xffff00});
        let sphere = new Mesh(geometry, material);
        this.viewer.scene.add(sphere);
    }

}