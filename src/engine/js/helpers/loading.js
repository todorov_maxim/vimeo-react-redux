import loadingIcon from '../../assets/img/loader.gif';

export default class Loading{
	constructor(container){
		this.container = container;
		this.init();
	}

	init(){
		this.svg = document.createElement('img');
		this.svg.src = loadingIcon;
		this.svg.className = 'svg-loader';
		this.container.appendChild(this.svg);
		//this.show();
	}

	show(){
		this.svg.classList.add('show-svg');
		this.container.style.pointerEvents = 'none';
	}

	hide() {
		this.svg.classList.remove('show-svg');
		this.container.style.pointerEvents = 'auto';
	}
}