import {createReducer, createAction} from "redux-act";

export const setServerMessage = createAction('setUser');

const server = createReducer({
    [setServerMessage]: (state,payload) => payload
},{});

export default server;