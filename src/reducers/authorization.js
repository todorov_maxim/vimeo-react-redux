import {createReducer, createAction} from "redux-act";

export const setUser = createAction('setUser');

const user = createReducer({
    [setUser]: (state,payload) => payload
},{});

export default user;