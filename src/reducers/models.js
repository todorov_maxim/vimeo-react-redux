import {createReducer, createAction} from "redux-act";

export const setModelsList = createAction('setModelsList');

const models = createReducer({
    [setModelsList]: (state,payload) => payload
},{});

export default models;