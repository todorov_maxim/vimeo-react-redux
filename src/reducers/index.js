import {combineReducers} from 'redux'
import {routerReducer} from 'react-router-redux'
import user from './authorization';
import server from './server';
import models from './models';

export default combineReducers({
    user,
    server,
    models,
    routing: routerReducer
});