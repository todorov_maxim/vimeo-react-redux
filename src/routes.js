import React from 'react';
import {Route} from 'react-router-dom';
import Home from './components/Home/index';
import Registration from './components/Registration/index'
import Login from './components/Login';
import Header from './containers/Header'
import CustomRoutes from './containers/CustromRoute';

export const routes = [
    <CustomRoutes exact key='1' path='/' component={Home} hideHeader = {false} />,
    <CustomRoutes exact key='2' path='/registration' component={Registration}/>,
    <CustomRoutes exact key='3' path='/login' component={Login}/>,
];
