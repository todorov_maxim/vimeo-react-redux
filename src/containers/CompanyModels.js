import {connect} from 'react-redux'
import CompanyModels from '../components/common/CompanyModels';
import {withRouter} from "react-router-dom";
import {getModels} from './../action/models';


export default withRouter(connect(
    state => ({
    }),
    dispatch => ({
        getModels: (id) => dispatch(getModels(id))
    })
)(CompanyModels));