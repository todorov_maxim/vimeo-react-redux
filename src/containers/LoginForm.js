import {connect} from 'react-redux'
import LoginForm from '../components/common/LoginForm';
import {signIn} from '../action/authorization';

export default connect(
    state => ({
        user: state.user
    }),
    dispatch => ({
        signIn: (data) => dispatch(signIn(data))
    })
)(LoginForm);