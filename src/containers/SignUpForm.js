import {connect} from 'react-redux'
import SignUpForm from '../components/common/SignUpForm';
import {signUp} from  '../action/authorization';

export default connect(
    state => ({
        server: state.server
    }),
    dispatch => ({
        signUp: (data)=>dispatch(signUp(data))
    })
)(SignUpForm);