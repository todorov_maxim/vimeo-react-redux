import {connect} from 'react-redux'
import Header from '../components/Header/index';
import {withRouter} from "react-router-dom";
import {auth} from './../action/authorization';


export default withRouter(connect(
    state => ({
        user: state.user,
    }),
    dispatch => ({
        auth: () => dispatch(auth())
    })
)(Header));