import React from 'react';
import ReactDOM from 'react-dom';
import {createStore, applyMiddleware, compose} from 'redux';
import {Provider} from 'react-redux';
import thunk from 'redux-thunk';
import store from './reducers/index';
import {Router, Switch} from "react-router-dom";
import {routes} from './routes';
import createBrowserHistory from 'history/createBrowserHistory';
import {routerMiddleware} from 'react-router-redux';


const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const createHistory = createBrowserHistory();
const Store = createStore(store, composeEnhancers(applyMiddleware(thunk, routerMiddleware(createHistory))));


ReactDOM.render(<Provider store={Store}>
                    <Router history={createHistory}>
                        <Switch> {routes} </Switch>
                    </Router>
                </Provider>,
    document.getElementById('root'));


