import {push} from 'react-router-redux';
import {signUp as signUpPost} from './../api/authorization'
import {signIn as signInPost} from './../api/authorization'
import {auth as authGet} from "./../api/authorization";
import {setUser} from './../reducers/authorization';
import {setServerMessage} from "../reducers/server";


export const signUp = (userData) => (dispatch) => {
    return signUpPost(userData).then((res) => {
        if(res.success) {
            return dispatch(push('/login'));
        }else{
            return dispatch(setServerMessage({'error': res.message}));
        }
    });
};

export const signIn = (userData) => (dispatch) => {
    return signInPost(userData).then((res) => {
        console.log('res', res);
        if (res.status === 200) {
            dispatch(setUser({auth: true}));
            return dispatch(push('/'));
        } else {
            dispatch(setUser({'error': res.statusText}));
            return res.statusText;
        }
    });
};

export const auth = () => (dispatch) => {
    return authGet().then((res)=>{
        console.log(res);
    });
};