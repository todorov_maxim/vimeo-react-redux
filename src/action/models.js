import {push} from 'react-router-redux';
import {getModels as getModelsGet} from "../api/models";

export const getModels = (id) => (dispatch) => {
    return getModelsGet(id).then((res) => {
       console.log('Action', res);
    });
};