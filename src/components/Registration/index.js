import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import logo from '../../assets/img/header/vumia.png';
import SignUpForm  from '../../containers/SignUpForm';
import './index.scss';


class Registration extends Component {

    state = {
        registerVariant: null
    };

    setRegisterVarient = (type) => () => {
        return this.setState({
            registerVariant: type
        });
    };

    render() {
        return (
            <div className='registration'>
                <div className='wellcome-block'>
                    <span className='title'>Welcome to <img src={logo} alt="vumia"/></span>
                    <span className='secondary'>Join the leading showcase platform for 3D viewer models</span>
                    <div className='registration-state'>
                        <div className='point'>
                            <div className='bounce active'></div>
                            <span className='label'>Type</span>
                        </div>
                        <div className='point'>
                            <div className='bounce active'></div>
                            <span className='label'>Type</span>
                        </div>
                        <div className='point'>
                            <div className='bounce active'></div>
                            <span className='label'>Type</span>
                        </div>
                        <div className='line'>
                        </div>
                    </div>
                </div>
                {
                    this.state.registerVariant ?

                        <SignUpForm type={this.state.registerVariant} />

                        :
                        <div className='type'>
                            <span>Who are you?</span>
                            <div className='individual-type' onClick={this.setRegisterVarient('individual')}>
                                <img src="" alt="heart"/>
                                <span>Invidual</span>
                                <p>bla-bla-bla-bla</p>
                            </div>
                            <div className='company-type' onClick={this.setRegisterVarient('company')}>
                                <img src="" alt="sun"/>
                                <span>Company</span>
                                <p>bla-bla-bla-bla</p>
                            </div>

                            <span>Already have an account? <Link to='/login'> Log in here</Link></span>
                        </div>

                }


            </div>


        )
    }
}

export default Registration;