import React, {Component} from 'react';
import logo from '../../assets/img/header/vumia.png';
import LoginForm  from '../../containers/LoginForm';
import { Link } from 'react-router-dom'
import './index.scss';


class Login extends Component {
    render() {
        return (
            <div className='login'>
                <img src={logo} alt=""/>
                <p>Nice to see you on our site</p>
                <p>Sign in</p>
                <LoginForm/>
                <p>New to Vumia? <Link to="/registration">Sign up here</Link></p>


            </div>
        )
    }
}

export default Login;