import React from 'react';
import {Route} from 'react-router-dom';
import Header from '../../../containers/Header';

const CustomRoute = ({component: Component, hideHeader}) => {
    return (
        <Route render={props => (
            <div>
                {
                    hideHeader ? null : <Header/>
                }
                <Component {...props} />
            </div>
        )}>


        </Route>
    )
};


export default CustomRoute;