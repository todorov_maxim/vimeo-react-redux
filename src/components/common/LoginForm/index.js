import React, {Component} from 'react';


class LoginForm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            userData: {
                email: '',
                password: ''
            },
            errors: {},
            errorMsg: null
        };
    }


    handleSubmit = (event) => {
        event.preventDefault();

        let errors = {};
        const userData = this.state.userData;
        this.setState({errors, errorMsg: null});

        for (let v in userData) {
            if (userData[v] === '') {
                errors = {...errors, [v]: true};
                this.setState({errors, errorMsg: 'fields is requierment'})
            }
        }


        if (Object.keys(errors).length > 0) {
            return null;
        }

        const reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        if (!reg.test(this.state.userData.email)) {
            return this.setState({
                errors: {email: true},
                errorMsg: 'Please, use another email.'
            })
        }


        const {email, password} = userData;

        this.props.signIn({email, password});

    };

    handleChange = (type) => (event) => {

        return this.setState({
            errors: {},
            errorMsg: null,
            userData: {
                ...this.state.userData,
                [type]: event.target.value
            }
        });

    };


    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <span>Email</span>
                <input type="text" name='email' onChange={this.handleChange('email')}
                       value={this.state.userData.email}/>
                {this.state.errors.email && <span>{this.state.errorMsg}</span>}


                <span>Password</span>
                <input type="text" name='password' onChange={this.handleChange('password')}
                       value={this.state.userData.password}/>
                {this.state.errors.password && <span>{this.state.errorMsg}</span>}

                <button type='submit'>SIGN IN</button>

                {
                    this.props.user.error ?
                        <span>{this.props.user.error}</span> : null
                }
            </form>
        )
    }


}

export default LoginForm;