import React, {Component} from 'react';
import objectToFormData from 'object-to-formdata';

class SignUpForm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            userData: {
                type: this.props.type === 'individual' ? 1 : 2,
                email: '',
                company: '',
                brand: '',
                country: '',
                city: '',
                zip: '',
                address: '',
                username: '',
                password: '',
                retypePassword: '',
                logo: '',
                firstName: '',
                middleName: '',
                lastName: ''
            },
            password_check: '',
            step: 1,
            errors: {},
            errorMsg: null
        };

        this.reviewCompanyArr = ['company', 'country', 'city', 'zip', 'address', 'username', 'logo'];
        this.reviewIndividualArr = ['country', 'city', 'zip', 'address', 'username', 'firstName', 'middleName', 'lastName'];
    }

    handleSubmit = (event) => {
        event.preventDefault();

        const reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        if(!reg.test(this.state.userData.email)){
            return this.setState({errorMsg: 'Please, use another email.'})
        }

        if(this.state.userData.password !== this.state.userData.retypePassword || this.state.userData.password.length < 6 ){
            return this.setState({errorMsg: 'Please, check your password.'})
        }


        //temp code
        this.state.userData.brand = this.state.userData.company;

        // temp code - end

        const formData = objectToFormData(this.state.userData);

        this.props.signUp(formData);

    };

    handleChange = () => (event) => {
        console.log(this.state.userData);

        return this.setState({
            userData: {
                ...this.state.userData,
                [event.target.name]: event.target.files ? event.target.files[0] : event.target.value
            },
            errors: {},
            errorMsg: null
        })
    };

    changeStep = () => {

        // Check all fields //
        let errors = {};
        const userData = this.state.userData;
        this.setState({errors, errorMsg: null});
        let review = this.props.type === 'individual' ? this.reviewIndividualArr : this.reviewCompanyArr;

        for (let i = 0; i < review.length; i++) {
            if (userData[review[i]] === '') {
                errors = {...errors, [i]: true};
                this.setState({errors, errorMsg: 'All fields is requierment'})
            }
        }

        if (Object.keys(errors).length > 0) {
            return null;
        }
        // Check all fields - END //

        return this.setState({
            step: 2
        });
    };

    render() {
        console.log(this.state);
        return (
            <form onSubmit={this.handleSubmit}>

                {
                    this.state.step === 1 ?
                        this.state.userData.type === 1 ?
                            <div>
                                <span>First name</span>
                                <input name='firstName' type="text" onChange={this.handleChange()}/>
                                <span>Middle name</span>
                                <input name='middleName' type="text" onChange={this.handleChange()}/>
                                <span>Last name</span>
                                <input name='lastName' type="text" onChange={this.handleChange()}/>
                                <span>User name</span>
                                <input name='username' type="text" onChange={this.handleChange()}/>
                                <span>Country</span>
                                <input name='country' type="text" onChange={this.handleChange()}/>
                                <span>City</span>
                                <input name='city' type="text" onChange={this.handleChange()}/>
                                <span>Zip code</span>
                                <input name='zip' type="text" onChange={this.handleChange()}/>
                                <span>Address</span>
                                <input name='address' type="text" onChange={this.handleChange()}/>

                                <div onClick={this.changeStep}>Next</div>
                            </div>
                            :
                            <div>
                                <input name='logo' type="file" onChange={this.handleChange()}/>
                                <span>Company name</span>
                                <input name='company' type="text" onChange={this.handleChange()}/>
                                <span>User name</span>
                                <input name='username' type="text" onChange={this.handleChange()}/>
                                <span>Brand name</span>
                                <input disabled={true} name='brand' type="text" onChange={this.handleChange()}
                                       value={this.state.userData.company}/>
                                <span>Country</span>
                                <input name='country' type="text" onChange={this.handleChange()}/>
                                <span>City</span>
                                <input name='city' type="text" onChange={this.handleChange()}/>
                                <span>Zip code</span>
                                <input name='zip' type="text" onChange={this.handleChange()}/>
                                <span>Address</span>
                                <input name='address' type="text" onChange={this.handleChange()}/>

                                <div onClick={this.changeStep}>Next</div>
                            </div>
                        :
                        <div>
                            <span>Email</span>
                            <input name='email' type="text" onChange={this.handleChange()}/>

                            <span>Password</span>
                            <input name='password' type="text" onChange={this.handleChange()}/>

                            <span>Retype Password</span>
                            <input name='retypePassword' type="text" onChange={this.handleChange()}/>

                            <button type='submit'>Sign Up</button>
                        </div>
                }


                {
                    this.state.errorMsg ?
                        <span>{this.state.errorMsg}</span>
                        :
                        null
                }

                {
                    this.props.server.error ?
                        <span>{this.props.server.error}</span>
                        :
                        null
                }
            </form>
        )
    }


}

export default SignUpForm;