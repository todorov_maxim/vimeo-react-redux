import React, {Component} from 'react';
import logo from '../../assets/img/header/vumia.png';
import {Link} from 'react-router-dom';
import './index.scss';

class Header extends Component {


    render() {

        console.log(this.props);

        return (
            <header>
                <div className='menu'>
                    <img src={logo} alt="logo"/>
                    <span className='tab'>About us</span>
                    <span className='tab'>Politics</span>
                    <span className='tab'>FAQ</span>
                </div>

                {
                    this.props.user.auth ?
                        <div>Auth: TRUE</div>
                        :
                        <div className='auth'>
                            <Link to='/login'>
                                <div className='auth-button sign-in'>SIGN IN</div>
                            </Link>
                            <Link to='/registration'>
                                <div className='auth-button sign-up'>SIGN UP</div>
                            </Link>
                        </div>
                }

            </header>
        )
    }
}

export default Header;