
export const signUp = (data) => {
    return fetch('http://localhost:9099/signUp', {
        method: 'post',
        body: data
    }).then((res) => {
        return res.json();
    }).then((json)=>{
        return json;
    });

};

export const signIn = (data) => {
    return fetch('http://localhost:9099/login', {
        method: 'post',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify(data)
    }).then((res) => {
        return res;
    });
};

export const auth = () => {
    return fetch('http://localhost:9099/check', {
        method: 'get',
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    }).then((res) => {
        return res;
    });
};